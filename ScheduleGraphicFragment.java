package com.vdu.studentas.ui.schedule.graphic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vdu.studentas.R;
import com.vdu.studentas.ui.classes.ClassModel;
import com.vdu.studentas.ui.classes.GetClasses;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * {@link Fragment} for displaying VDU class schedule in graphic format.
 */
public class ScheduleGraphicFragment extends Fragment
{
	ArrayList<WeekViewEvent<ScheduleWeekEventModel>> weekViewEvents;
	WeekView<ScheduleWeekEventModel> mWeekView;
	private DatabaseReference scheduleReference;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_schedule_graphic, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		FirebaseAuth mAuth = FirebaseAuth.getInstance();
		if (mAuth.getCurrentUser() == null)
		{
			Navigation.findNavController(view).navigate(R.id.action_nav_logout);
			return;
		}

		scheduleReference = FirebaseDatabase.getInstance().getReference().child("schedule").child(Objects.requireNonNull(mAuth.getUid())).child("weeks");

		// Get a reference for the week view in the layout.
		mWeekView = view.findViewById(R.id.schedule_weekView);
		// Set hour range
		mWeekView.setMinHour(8);
		mWeekView.setMaxHour(20);

		Tasks.<Void>forResult(null).continueWithTask(new GetClasses()).continueWithTask(new ScheduleGraphicFragment.GetSchedule()).addOnSuccessListener(result ->
		{
			weekViewEvents = result;

			// Load Schedule
			mWeekView.submit(weekViewEvents);
		});

		AppCompatImageButton toListButton = view.findViewById(R.id.schedule_to_list_button);
		toListButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_nav_schedule_graphic_to_nav_schedule_list));
	}

	private class GetSchedule implements Continuation<HashMap<String, ClassModel>, Task<ArrayList<WeekViewEvent<ScheduleWeekEventModel>>>>
	{
		@Override
		public Task<ArrayList<WeekViewEvent<ScheduleWeekEventModel>>> then(@NonNull Task<HashMap<String, ClassModel>> task)
		{
			final HashMap<String, ClassModel> classModels = task.getResult();
			final TaskCompletionSource<ArrayList<WeekViewEvent<ScheduleWeekEventModel>>> tcs = new TaskCompletionSource<>();

			scheduleReference.addValueEventListener(new ValueEventListener()
			{
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot)
				{
					ArrayList<WeekViewEvent<ScheduleWeekEventModel>> ScheduleWeekEventModelEvents = new ArrayList<>();

					for (DataSnapshot week : dataSnapshot.getChildren())
					{
						int id = 0;
						for (DataSnapshot day : week.child("days").getChildren())
						{
							ClassModel classModel = classModels.get(day.child("class").getValue(String.class));

							LocalDate localDate = new LocalDate(week.child("year").getValue(Integer.class),
									week.child("month").getValue(Integer.class),
									day.child("day_of_month").getValue(Integer.class));

							ScheduleWeekEventModelEvents.add(new ScheduleWeekEventModel(classModel.getName(),
									new LocalTime(day.child("start_time").getValue(String.class)),
									new LocalTime(day.child("end_time").getValue(String.class)),
									localDate,
									id++).toWeekViewEvent());
						}
					}

					mWeekView.notifyDataSetChanged();
					tcs.setResult(ScheduleWeekEventModelEvents);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError)
				{
					tcs.setException(databaseError.toException());
				}
			});

			return tcs.getTask();
		}
	}
}