package com.vdu.studentas.ui.grades;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vdu.studentas.R;
import com.vdu.studentas.ui.classes.ClassModel;
import com.vdu.studentas.ui.classes.GetClasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * {@link Fragment} for displaying on-going semester grades.
 */
public class GradesFragment extends Fragment
{
	ArrayList<GradesRecyclerItemModel> itemModels;
	DatabaseReference gradesReference;
	GradesRecyclerViewAdapter recyclerViewAdapter;
	private NavController navController;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_grades, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		navController = Navigation.findNavController(view);
		AppCompatImageButton toResultsButton = view.findViewById(R.id.grades_to_results_button);
		toResultsButton.setOnClickListener(v ->
		{
			navController.navigate(R.id.action_nav_grades_to_nav_results);
		});

		FirebaseAuth mAuth = FirebaseAuth.getInstance();

		if (mAuth.getCurrentUser() == null)
		{
			navController.navigate(R.id.action_nav_logout);
			return;
		}

		FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
		DatabaseReference rootReference = mDatabase.getReference();
		gradesReference = rootReference.child("grades").child(Objects.requireNonNull(mAuth.getUid())).child("grades");

		Tasks.<Void>forResult(null).continueWithTask(new GetClasses()).continueWithTask(new GetGrades()).addOnSuccessListener(result ->
		{
			RecyclerView recyclerView = view.findViewById(R.id.grades_recyclerView);
			itemModels = result;
			recyclerViewAdapter = new GradesRecyclerViewAdapter(itemModels);
			recyclerView.setAdapter(recyclerViewAdapter);

			FragmentActivity fragmentActivity = requireActivity();
			recyclerView.setLayoutManager(new LinearLayoutManager(fragmentActivity));
			recyclerView.addItemDecoration(new DividerItemDecoration(fragmentActivity, DividerItemDecoration.VERTICAL));
			recyclerViewAdapter.notifyDataSetChanged();
		});


	}

	/**
	 * Task that returns VDU grades from database as {@link ArrayList} of {@link GradesRecyclerItemModel}.
	 */
	private class GetGrades implements Continuation<HashMap<String, ClassModel>, Task<ArrayList<GradesRecyclerItemModel>>>
	{

		@Override
		public Task<ArrayList<GradesRecyclerItemModel>> then(@NonNull Task<HashMap<String, ClassModel>> task)
		{
			final HashMap<String, ClassModel> classModels = task.getResult();
			final TaskCompletionSource<ArrayList<GradesRecyclerItemModel>> tcs = new TaskCompletionSource<>();

			gradesReference.addValueEventListener(new ValueEventListener()
			{
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot)
				{
					ArrayList<GradesRecyclerItemModel> gradesRecyclerItemModels = new ArrayList<>();
					for (DataSnapshot ds : dataSnapshot.getChildren())
					{
						gradesRecyclerItemModels.add(new GradesRecyclerItemModel(Objects.requireNonNull(classModels.get(ds.child("class").getValue(String.class))).getName(),
								ds.child("task").getValue(String.class),
								ds.child("grade").getValue(Integer.class)));
					}

					tcs.setResult(gradesRecyclerItemModels);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError)
				{
					tcs.setException(databaseError.toException());
				}
			});

			return tcs.getTask();
		}
	}

}