package com.vdu.studentas.ui.results;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vdu.studentas.R;
import com.vdu.studentas.ui.classes.ClassModel;
import com.vdu.studentas.ui.classes.GetClasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * {@link Fragment} for displaying final semester grades for each semester in the database.
 */
public class ResultsFragment extends Fragment
{
	DatabaseReference resultsReference;
	private ArrayList<ResultsPagerModel> pagerModels;
	private NavController navController;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_results, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		navController = Navigation.findNavController(view);

		FirebaseAuth mAuth = FirebaseAuth.getInstance();
		if (mAuth.getCurrentUser() == null)
		{
			navController.navigate(R.id.action_nav_logout);
			return;
		}

		FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
		DatabaseReference rootReference = mDatabase.getReference();
		resultsReference = rootReference.child("results").child(Objects.requireNonNull(mAuth.getUid())).child("results");

		// Instantiate a ViewPager2 and a PagerAdapter.
		/**
		 * The pager widget, which handles animation and allows swiping horizontally to access previous
		 * and next wizard steps.
		 */

		AppCompatImageButton toGradesButton = view.findViewById(R.id.results_to_grades_button);
		toGradesButton.setOnClickListener(v ->
		{
			navController.navigate(R.id.action_nav_results_to_nav_grades);
		});

		Tasks.<Void>forResult(null).continueWithTask(new GetClasses()).continueWithTask(new GetResults()).addOnSuccessListener(result ->
		{
			ViewPager2 viewPager = view.findViewById(R.id.results_viewPager);
			pagerModels = result;
			FragmentStateAdapter pagerAdapter = new ResultsPagerAdapter(requireActivity(), pagerModels);
			viewPager.setAdapter(pagerAdapter);

			TabLayout tabLayout = view.findViewById(R.id.results_tabLayout);
			new TabLayoutMediator(tabLayout, viewPager,
					(tab, position) ->
					{
						ResultsPagerModel currentModel = pagerModels.get(position);
						tab.setText(String.format("%s %s", currentModel.getSemester(), currentModel.getYear()));
					}).attach();

			pagerAdapter.notifyDataSetChanged();
		});
	}

	private String gradeDescription(int grade)
	{
		switch (grade)
		{
			case 1:
				return getResources().getString(R.string.grade_1);
			case 2:
				return getResources().getString(R.string.grade_2);
			case 3:
				return getResources().getString(R.string.grade_3);
			case 4:
				return getResources().getString(R.string.grade_4);
			case 5:
				return getResources().getString(R.string.grade_5);
			case 6:
				return getResources().getString(R.string.grade_6);
			case 7:
				return getResources().getString(R.string.grade_7);
			case 8:
				return getResources().getString(R.string.grade_8);
			case 9:
				return getResources().getString(R.string.grade_9);
			case 10:
				return getResources().getString(R.string.grade_10);
			default:
				return "error";
		}
	}

	/**
	 * Task that returns VDU final semester grades from database as {@link ArrayList} of {@link ResultsPagerModel}.
	 */
	private class GetResults implements Continuation<HashMap<String, ClassModel>, Task<ArrayList<ResultsPagerModel>>>
	{

		@Override
		public Task<ArrayList<ResultsPagerModel>> then(@NonNull Task<HashMap<String, ClassModel>> task)
		{
			final HashMap<String, ClassModel> classModels = task.getResult();
			final TaskCompletionSource<ArrayList<ResultsPagerModel>> tcs = new TaskCompletionSource<>();

			resultsReference.addValueEventListener(new ValueEventListener()
			{
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot)
				{
					ArrayList<ResultsPagerModel> resultPagerModels = new ArrayList<>();
					for (DataSnapshot children : dataSnapshot.getChildren())
					{
						ArrayList<SingleResultRecyclerItemModel> singleResultRecyclerItemModels = new ArrayList<>();

						for (DataSnapshot classes : children.child("classes").getChildren())
						{
							int grade = classes.child("grade").getValue(Integer.class);
							singleResultRecyclerItemModels.add(new SingleResultRecyclerItemModel(Objects.requireNonNull(classModels.get(classes.child("class").getValue(String.class))).getName(),
									grade + " " + gradeDescription(grade),
									classes.child("credits").getValue(Integer.class)));
						}

						DataSnapshot semester = children.child("semester");
						ResultsPagerModel resultsPagerModel = new ResultsPagerModel(semester.child("year").getValue(Integer.class),
								semester.child("season").getValue(String.class),
								singleResultRecyclerItemModels);

						resultPagerModels.add(resultsPagerModel);
					}

					tcs.setResult(resultPagerModels);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError)
				{
					tcs.setException(databaseError.toException());
				}
			});

			return tcs.getTask();
		}
	}
}