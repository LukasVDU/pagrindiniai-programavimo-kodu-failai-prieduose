package com.vdu.studentas.ui.schedule.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vdu.studentas.R;
import com.vdu.studentas.ui.classes.ClassModel;
import com.vdu.studentas.ui.classes.GetClasses;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * {@link Fragment} for displaying VDU class schedule in list format.
 */
public class ScheduleListFragment extends Fragment
{
	private ArrayList<ScheduleListWeekModel> pagerModels;
	private DatabaseReference scheduleReference;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{

		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_schedule_list, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		NavController navController = Navigation.findNavController(view);

		FirebaseAuth mAuth = FirebaseAuth.getInstance();
		if (mAuth.getCurrentUser() == null)
		{
			navController.navigate(R.id.action_nav_logout);
			return;
		}

		scheduleReference = FirebaseDatabase.getInstance().getReference().child("schedule").child(Objects.requireNonNull(mAuth.getUid())).child("weeks");

		AppCompatImageButton toGraphicButton = view.findViewById(R.id.schedule_to_graphic_button);
		toGraphicButton.setOnClickListener(v -> navController.navigate(R.id.action_nav_schedule_list_to_nav_schedule_graphic));

		Tasks.<Void>forResult(null).continueWithTask(new GetClasses()).continueWithTask(new ScheduleListFragment.GetSchedule()).addOnSuccessListener(result ->
		{
			// Instantiate a ViewPager2 and a PagerAdapter.
			/**
			 * The pager widget, which handles animation and allows swiping horizontally to access previous
			 * and next wizard steps.
			 */
			ViewPager2 viewPager = view.findViewById(R.id.schedule_viewPager);
			pagerModels = result;
			FragmentStateAdapter pagerAdapter = new ScheduleListPagerAdapter(requireActivity(), pagerModels);
			viewPager.setAdapter(pagerAdapter);

			TabLayout tabLayout = view.findViewById(R.id.schedule_tabLayout);
			new TabLayoutMediator(tabLayout, viewPager,
					(tab, position) ->
					{
						ScheduleListWeekModel currentModel = pagerModels.get(position);
						tab.setText(String.format("%s %d - %d", currentModel.weekStartDate.monthOfYear().getAsText(), currentModel.weekStartDate.getDayOfMonth(), currentModel.weekEndDate.getDayOfMonth()));
					}).attach();

			pagerAdapter.notifyDataSetChanged();
		});
	}

	private class GetSchedule implements Continuation<HashMap<String, ClassModel>, Task<ArrayList<ScheduleListWeekModel>>>
	{

		@Override
		public Task<ArrayList<ScheduleListWeekModel>> then(@NonNull Task<HashMap<String, ClassModel>> task)
		{
			final HashMap<String, ClassModel> classModels = task.getResult();
			final TaskCompletionSource<ArrayList<ScheduleListWeekModel>> tcs = new TaskCompletionSource<>();

			scheduleReference.addValueEventListener(new ValueEventListener()
			{
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot)
				{
					ArrayList<ScheduleListWeekModel> ScheduleListWeekModels = new ArrayList<>();

					for (DataSnapshot week : dataSnapshot.getChildren())
					{
						LocalDate localDate = new LocalDate(week.child("year").getValue(Integer.class),
								week.child("month").getValue(Integer.class),
								1);

						ArrayList<ScheduleListDayModel> scheduleListDayModels = new ArrayList<>();

						for (DataSnapshot day : week.child("days").getChildren())
						{
							ClassModel classModel = classModels.get(day.child("class").getValue(String.class));

							scheduleListDayModels.add(new ScheduleListDayModel(classModel.getName(),
									new LocalTime(day.child("start_time").getValue(String.class)),
									new LocalTime(day.child("end_time").getValue(String.class)),
									new LocalDate(localDate.dayOfMonth().setCopy(day.child("day_of_month").getValue(Integer.class))),
									classModel.getFullId(),
									classModel.getLocation()));
						}

						ScheduleListWeekModels.add(new ScheduleListWeekModel(new LocalDate(localDate.dayOfMonth().setCopy(week.child("week_start_day").getValue(Integer.class))),
								new LocalDate(localDate.dayOfMonth().setCopy(week.child("week_end_day").getValue(Integer.class))),
								scheduleListDayModels));
					}

					tcs.setResult(ScheduleListWeekModels);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError)
				{
					tcs.setException(databaseError.toException());
				}
			});

			return tcs.getTask();
		}
	}
}