package com.vdu.studentas.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vdu.studentas.MainActivity;
import com.vdu.studentas.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;
import java.util.Objects;

/**
 * {@link Fragment} for displaying app settings.
 */
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener
{
	// notificationId is a unique int for each notification that you must define
	static int notificationId = 1;

	static FirebaseAuth mAuth;

	static ValueEventListener gradesValueEventListener;
	static ValueEventListener newsValueEventListener;
	static DatabaseReference gradesReference;
	static DatabaseReference newsReference;

	public static void SetNotifications(Context context)
	{
		NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
				.setSmallIcon(R.drawable.vdu_logo)
				.setContentTitle("Student VDU Notification")
				.setPriority(NotificationCompat.PRIORITY_DEFAULT);

		FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
		DatabaseReference rootReference = mDatabase.getReference();
		mAuth = FirebaseAuth.getInstance();
		gradesReference = rootReference.child("grades").child(Objects.requireNonNull(mAuth.getUid())).child("grades");
		newsReference = rootReference.child("news");

		DateTime lastLoginDate = new DateTime(Objects.requireNonNull(Objects.requireNonNull(mAuth.getCurrentUser()).getMetadata()).getLastSignInTimestamp());

		gradesValueEventListener = new ValueEventListener()
		{
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot)
			{
				DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

				for (DataSnapshot ds : dataSnapshot.getChildren())
				{
					DateTime insertDate = dtf.parseDateTime(Objects.requireNonNull(ds.child("timestamp").getValue(String.class)));

					if (lastLoginDate.getMillis() < insertDate.getMillis())
					{
						notificationBuilder.setContentText("New grades added");
						notificationManager.notify(notificationId++, notificationBuilder.build());
						return;
					}
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError)
			{

			}
		};
		gradesReference.addValueEventListener(gradesValueEventListener);

		newsValueEventListener = new ValueEventListener()
		{
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot)
			{
				DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy.MM.dd");

				for (DataSnapshot ds : dataSnapshot.getChildren())
				{
					DateTime insertDate = dtf.parseDateTime(Objects.requireNonNull(ds.child("date").getValue(String.class)));

					if (lastLoginDate.getMillis() < insertDate.getMillis())
					{
						notificationBuilder.setContentText("New article added");
						notificationManager.notify(notificationId++, notificationBuilder.build());
						return;
					}
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError)
			{

			}
		};

		newsReference.addValueEventListener(newsValueEventListener);
	}

	public static void RemoveNotifications()
	{
		gradesReference.removeEventListener(gradesValueEventListener);
		newsReference.removeEventListener(newsValueEventListener);
		notificationId = 0;
	}

	private void setLocale(String language)
	{
		Configuration mainConfig = new Configuration(getResources().getConfiguration());
		mainConfig.setLocale(new Locale(Objects.requireNonNull(language)));
		getResources().updateConfiguration(mainConfig, null);
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
	{
		setPreferencesFromResource(R.xml.root_preferences, rootKey);

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
		setLocale(sharedPreferences.getString("language", "en"));
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
	}

	/**
	 * Called when a shared preference is changed, added, or removed. This
	 * may be called even if a preference is set to its existing value.
	 *
	 * <p>This callback will be run on your main thread.
	 *
	 * <p><em>Note: This callback will not be triggered when preferences are cleared via
	 * {@link SharedPreferences.Editor#clear()}.</em>
	 *
	 * @param sharedPreferences The {@link SharedPreferences} that received
	 *                          the change.
	 * @param key               The key of the preference that was changed, added, or
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		switch (key)
		{
			case "language":
				setLocale(sharedPreferences.getString(key, "en"));
				requireActivity().recreate();
				break;
			case "notifications":
				if (sharedPreferences.getBoolean(key, false))
				{
					Context context = getContext();
					if (context != null)
					{
						SetNotifications(context);
					}
				}
				else
				{
					RemoveNotifications();
				}
				break;
		}
	}
}