package com.vdu.studentas.ui.login;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.vdu.studentas.R;

import java.util.Objects;

/**
 * Fragment for displaying app login.
 * If user logged out then all other fragments will redirect here.
 */
public class LoginFragment extends Fragment
{

	private LoginViewModel loginViewModel;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState)
	{

		View view = inflater.inflate(R.layout.fragment_login, container, false);
		Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).hide();
		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory()).get(LoginViewModel.class);

		final EditText emailEditText = view.findViewById(R.id.email);
		final EditText passwordEditText = view.findViewById(R.id.password);
		final Button loginButton = view.findViewById(R.id.login);
		final ProgressBar loadingProgressBar = view.findViewById(R.id.loading);

		loginViewModel.getLoginFormState().observe(getViewLifecycleOwner(), loginFormState ->
		{
			if (loginFormState == null)
			{
				return;
			}
			loginButton.setEnabled(loginFormState.isDataValid());
			if (loginFormState.getUsernameError() != null)
			{
				emailEditText.setError(getString(loginFormState.getUsernameError()));
			}
			if (loginFormState.getPasswordError() != null)
			{
				passwordEditText.setError(getString(loginFormState.getPasswordError()));
			}
		});

		loginViewModel.getLoginResult().observe(getViewLifecycleOwner(), loginResult ->
		{
			if (loginResult == null)
			{
				return;
			}
			loadingProgressBar.setVisibility(View.GONE);
			if (loginResult.getError() != null)
			{
				onLoginFailed(loginResult.getError());
			}
			if (loginResult.getSuccess() != null)
			{
				onLoginSuccessful(loginResult.getSuccess());
			}
		});

		TextWatcher afterTextChangedListener = new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
				// ignore
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				// ignore
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				loginViewModel.loginDataChanged(emailEditText.getText().toString(), passwordEditText.getText().toString());
			}
		};
		emailEditText.addTextChangedListener(afterTextChangedListener);
		passwordEditText.addTextChangedListener(afterTextChangedListener);
		passwordEditText.setOnEditorActionListener((v, actionId, event) ->
		{
			if (actionId == EditorInfo.IME_ACTION_DONE)
			{
				loginViewModel.login(requireActivity(), emailEditText.getText().toString(), passwordEditText.getText().toString());
			}
			return false;
		});

		loginButton.setOnClickListener(v ->
		{
			loadingProgressBar.setVisibility(View.VISIBLE);
			loginViewModel.login(requireActivity(), emailEditText.getText().toString(), passwordEditText.getText().toString());
		});
	}

	private void onLoginSuccessful(LoggedInUserView model)
	{
		Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(R.id.action_nav_login_to_nav_home);
	}

	private void onLoginFailed(@StringRes Integer errorString)
	{
		if (getContext() != null && getContext().getApplicationContext() != null)
		{
			Toast.makeText(
					getContext().getApplicationContext(),
					errorString,
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onDestroyView()
	{
		Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).show();
		super.onDestroyView();
	}
}